import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {Club} from '../datamodel/Club';
import {Event} from '../datamodel/Event';
import {ManagementTeam} from '../datamodel/MgmtTeam';
import {InstructorFeedback} from '../datamodel/InstructorFeedback';
import { Router } from '@angular/router';
import { query } from '@angular/core/src/animation/dsl';
import { Attendance } from 'app/datamodel/Attendance';
import { createPipeInstance } from '@angular/core/src/view/provider';

@Injectable()
export class VirtuaGymService {

    public constructor(private http : Http,private router : Router) {}

    getClubs(list:string[]) : Promise<Club[]> {
     var url = "https://3xdv5lloi2.execute-api.us-east-2.amazonaws.com/v1/club?api_key=memaw5o2OUOCSF7lS2XhSFAltnNC6U1ivDerSKlGyj&club_secret=CS-19408-ACCESS-85mtogeK5RqBaxkNN5RVrCPLY";
     let headers: Headers = new Headers();
        headers.append("Authorization", "Basic " + "aGFtbWFkLmFmcmlkaUB0ZWNoY2VsZXIuY29tOmxpYnJhcnk="); 
        headers.append("Access-Control-Allow-Origin" , "*"); 
          headers.append("Access-Control-Allow-Headers" , "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"); 
          headers.append("Access-Control-Allow-Credentials" , 'true'); 

        return this.http 
        .get(url,{headers: headers})
        .toPromise()
        .then((data) => {
        
        console.log('extracting data now');
        let clubs : Club[] = [];
        let body = data.json();

        for (let entry of body.result) {
            let club = new Club();
            club.clubId = entry.id;
            club.clubName = entry.name;
            club.superClubId = entry.superclub_id;
            club.address = entry.formatted_address;
            
            if (club.clubName!=""){
                list.push(club.clubName);  
            }

            clubs.push(club);
          } 
      
          console.log(body);   
          console.log(clubs);   
        return clubs;
    }).catch(this.handleError);          
}

addClubTeamMember(newTeamMember: ManagementTeam){
    var url = "https://qtv21th0hd.execute-api.us-east-1.amazonaws.com/v1/club/management/add";
    let headers: Headers = new Headers();
    headers.append("Access-Control-Allow-Headers" , "access-control-allow-headers,content-type, Access-Control-Allow-Credentials,Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin,Access-Control-Allow-Headers"); 
    headers.append("Access-Control-Allow-Origin" , '*'); 
    return this.http 
        .post(url,newTeamMember,{headers: headers })
        .toPromise()
        .then((data) => { 

        console.log(newTeamMember);
        console.log(data);

          }).catch(this.handleError);
    }


removeClubTeamMember(mgrId: string){
    var url = "https://qtv21th0hd.execute-api.us-east-1.amazonaws.com/v1/club/management/remove?mgrId="+mgrId;
     let headers: Headers = new Headers();
    return this.http 
        .get(url)
        .toPromise()
        .then((data) => { 

        console.log(mgrId);
        console.log(data);

          }).catch(this.handleError);
}


checkFeedbackPresence (eventId: string) : Promise<boolean> {
    var url = "https://5btddpqi11.execute-api.us-east-1.amazonaws.com/v1/instructor-feedback/exist?eventId="+eventId;
    let headers: Headers = new Headers();
    //    headers.append("Authorization", "Basic " + "aGFtbWFkLmFmcmlkaUB0ZWNoY2VsZXIuY29tOmxpYnJhcnk="); 
        //headers.append("Access-Control-Allow-Origin" , "*"); 
        //headers.append("Access-Control-Allow-Headers" , "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"); 
        //headers.append("Access-Control-Allow-Credentials" , 'true'); 


    return this.http 
        .get(url,{headers: headers})
        .toPromise()
        .then((data) => { 

        console.log(eventId);
        console.log(data);
        console.log(data.json());

        if (data.json()==null)
            return false;
        else 
            return true;
          }).catch(this.handleError);
          
}


getClubMgmtTeam(clubId: number) : Promise<ManagementTeam[]> {
    //var url = "https://fdnthwv8ij.execute-api.us-east-2.amazonaws.com/v1/club/"+clubId+"/club-team";
    var url = "https://qtv21th0hd.execute-api.us-east-1.amazonaws.com/v1/club/"+clubId+"/club-team";
    let headers: Headers = new Headers();
    //    headers.append("Authorization", "Basic " + "aGFtbWFkLmFmcmlkaUB0ZWNoY2VsZXIuY29tOmxpYnJhcnk="); 
        headers.append("Access-Control-Allow-Origin" , "*"); 
        headers.append("Access-Control-Allow-Headers" , "access-control-allow-credentials,Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"); 
        headers.append("Access-Control-Allow-Credentials" , 'true'); 
     
        return this.http 
        .get(url)
        .toPromise()
        .then((data) => {
       console.log("check");
       console.log('extracting data now');
      
       let teams : ManagementTeam[] = [];
       let body = data.json();
       console.log(body);
       for (let entry of body) {
           let team = new ManagementTeam();
           
           team.clubId = entry.ClubID;
           team.firstName = entry.MFirstName;
           team.lastName = entry.MLastName;
           team.managerId = entry.ManagerID;
           team.position = entry.PosistionTitle;
           team.email = entry.MEmail;
           team.buildingId = entry.BuildingID;
           teams.push(team);
           
           console.log(teams);
       }
           return teams;

}).catch(this.handleError);  
        
}

getTest() {
    var url = "https://vsei5apfx8.execute-api.us-east-2.amazonaws.com/prod/attendance";
    let headers: Headers = new Headers();
    headers.append("Access-Control-Allow-Origin" , "*"); 
    headers.append("Access-Control-Allow-Headers" , "Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin"); 

    return this.http 
    .get(url,{headers: headers})
    .toPromise()
    .then((data) => {
    console.log('extracting data now'+data);
    console.log(data.json());
    var eventdata = data.json();
    console.log(eventdata.FEEDBACK_ID);
    console.log(eventdata.EVENT_ID);
    console.log(eventdata.NUMBER_OF_ATTENDEES);
    
}).catch(this.handleError);          
}


getClubEvents(clubId:number) : Promise<Event[]> {
    let events : Event[] = [];

    var end = new Date();
    var timestamp_end = Math.floor(end.getTime()/1000)  


    var start = new Date(end.getTime() - (6*( 7 * 24 * 60 * 60 * 1000)));
    var timestamp_start = Math.floor(start.getTime()/1000);
    var url = "https://3xdv5lloi2.execute-api.us-east-2.amazonaws.com/v1/event/club/"+clubId+"?timestamp_start="+timestamp_start+"&timestamp_end="+timestamp_end+"&api_key=memaw5o2OUOCSF7lS2XhSFAltnNC6U1ivDerSKlGyj&club_secret=CS-19408-ACCESS-85mtogeK5RqBaxkNN5RVrCPLY";
    
    let headers: Headers = new Headers();
    console.log("the url is:"+url);
    headers.append("Authorization", "Basic " + "aGFtbWFkLmFmcmlkaUB0ZWNoY2VsZXIuY29tOmxpYnJhcnk="); 

    return this.http 
    .get(url,{headers: headers})
    .toPromise()
    .then((data) => {
    
    console.log('extracting data now');
    console.log('Time StampStart');
    console.log(timestamp_start);    
    console.log('Time StampEnd');
    console.log(timestamp_end);
    let body = data.json();

    for (let entry of body.result) {
        let event = new Event();
        event.name = entry.title;
        event.start = entry.start.split(" ")[1];
        event.end = entry.end.split(" ")[1];
        event.eventType = entry.employee_note;
        event.date = entry.start.split(" ")[0];
        event.eventId = entry.event_id;

        //this.checkFeedbackPresence (event.eventId).then(c => event.presence = c);

        event.capacity = entry.max_places;
        events.push(event);
      } 
 
    console.log(events);
    return events;
}).catch(this.handleError);          
}

addInstructorFeedback(feedback:InstructorFeedback,start:string,end:string,date:string) : Promise<string> {

    var url = "https://5btddpqi11.execute-api.us-east-1.amazonaws.com/v1/instructor-feedback";
    let headers: Headers = new Headers();
    console.log("the url is:"+url);
    headers.append("Access-Control-Allow-Origin" , "*"); 
    headers.append("Access-Control-Allow-Headers" , "Origin, Accept, content-type, Authorization, Access-Control-Allow-Origin"); 

    headers.append("content-type", "application/json"); 

    console.log(feedback);   
    
    return this.http 
        .post(url,feedback)
        .toPromise()
        .then((data) => {
       
        this.router.navigate(['/instructor/submitted-feedback-form'] ,{ queryParams: { start:start,end:end,date:date,eventName:feedback.eventName,clubName:feedback.club } });
        console.log('extracting data now');    
        return "Success! Thank you for submitting this promptly."; 
        
      }).catch(this.failureMsg);
      
}

failureMsg(){
    return "Error in adding feedback!";
}

getAttendanceData(barChartData: any[] ,eventType:string,year:string,club:string) : Promise<any[]> {
  //  var url = "https://vsei5apfx8.execute-api.us-east-2.amazonaws.com/prod/attendance?";
    var url = "https://klfahw5vw1.execute-api.us-east-1.amazonaws.com/v1/attendance?";
    var queryString;
    console.log(barChartData);
    queryString = "year="+year;
    
    if (club!=null){
        queryString = queryString + "&club="+club;
    }

    if (eventType!=null){
        queryString = queryString+ "&eventType="+eventType;
    }

    let at = new Attendance();
    var urlWithQueryString = url + queryString;
     this.http 
        .get(urlWithQueryString)
        .toPromise()
        .then((data) => {
        console.log("string is"+urlWithQueryString);
        
        console.log('extracting data now');
        console.log(data);   
        var body= data.json();
        console.log(body);
        let index : number;
        for (let entry of body) {
            console.log("body");

            console.log(entry.month);
            if (entry.month.substring(0,1)==0){
                index = entry.month.substring(1,2);
            }
            else {
                index = entry.month;
            }
            console.log(index);
            at.classes[index-1] = entry.classes;
            at.attendance[index-1] = entry.attendance;
            at.aveAttendance[index-1] = entry.ave;
            at.capacity[index=1]=entry.capacity;
            console.log("attendance is"+entry.attendance);
            console.log("capacity is"+entry.capacity);
        }

        barChartData[0].data = at.classes;
        barChartData[1].data = at.attendance;
        barChartData[2].data = at.aveAttendance;
        barChartData[3].data = at.capacity;
        
        console.log(barChartData);
        //console.log(barChartData[3])

        return barChartData;
      }).catch(this.handleError);
      return null;
}


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);  
      return Promise.reject(error.message || error);
      }
    
}