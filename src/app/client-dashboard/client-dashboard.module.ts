import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientDashboardComponent } from './client-dashboard.component';

import { ClientDashboardRoutingModule } from './client-dashboard-routing.module';

const routes: Routes = [
  {
    path: '',
    component: ClientDashboardComponent,
    data: {
      title: 'Clients'
    }
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ClientDashboardModule { }

