import { Component,OnInit } from '@angular/core';
import { VirtuaGymService} from '../services/virtua-gym-service';
import { Attendance } from 'app/datamodel/Attendance';


@Component({
  templateUrl: 'client-dashboard.component.html'
})

export class ClientDashboardComponent implements OnInit{

constructor(private service : VirtuaGymService) { 
}

public barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true
};

  
public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
public barChartType = 'bar';
public barChartLegend = true;

public barChartData: any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
];

  public chartClicked(e: any): void {
      console.log(e);
  }
    
  public chartHovered(e: any): void {
      console.log(e);
  }
    
  ngOnInit(){
     // this.service.getAttendanceData(this.eventType,this.year,this.club).then(r => this.attendance = r);
 } 

}