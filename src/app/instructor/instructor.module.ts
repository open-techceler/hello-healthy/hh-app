import { NgModule } from '@angular/core';

import { InstructorComponent } from './instructor.component';
import { BranchComponent } from './branch.component';

import { InstructorRoutingModule } from './instructor-routing.module';
import { FeedbackComponent } from 'app/instructor/feedback-form.component';
import { SubmittedFeedbackFormComponent } from "app/instructor/submitted-feedback-form.component";
import { VirtuaGymService } from 'app/services/virtua-gym-service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';



@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    InstructorRoutingModule,    
    NgbModule.forRoot(),
    HttpModule,
  ],
  declarations: [
    InstructorComponent,
    FeedbackComponent,
   SubmittedFeedbackFormComponent
   ],
  providers: [VirtuaGymService],

})
export class InstructorModule { }
