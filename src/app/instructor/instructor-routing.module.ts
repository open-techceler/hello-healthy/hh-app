import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';

import { InstructorComponent } from './instructor.component';
import { FeedbackComponent } from './feedback-form.component';
import {SubmittedFeedbackFormComponent} from './submitted-feedback-form.component'

const routes: Routes = [
  {
    path: 'instructor',
    component: InstructorComponent,
    data: {
      title: 'Instructor'
    }
  },
  {
    path: 'feedback-form',
    component: FeedbackComponent,
    data: {
      title: 'Feedback Form'
    }
  }
   ,{
    path: 'submitted-feedback-form',
   component:SubmittedFeedbackFormComponent,
    data: {
      title: 'Form submitted'
    }
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstructorRoutingModule {}
