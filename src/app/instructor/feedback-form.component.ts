import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {InstructorFeedback} from '../datamodel/InstructorFeedback';
import { DatePipe } from '@angular/common';
import {VirtuaGymService} from '../services/virtua-gym-service';
import {Club} from '../datamodel/Club';
import {Event} from '../datamodel/Event';
import 'rxjs/util/isNumeric'
import { isNumeric } from 'rxjs/util/isNumeric';

@Component({
  templateUrl: 'feedback-form.component.html'
})
export class FeedbackComponent implements OnInit {

  constructor(private service : VirtuaGymService,private route: ActivatedRoute) {
    this.feedback  = new InstructorFeedback();
  }
  
  clubs : Club[] = [];
  events : Event[] = [];
 
  feedback: InstructorFeedback;
  eventName : string;
  clubName : string;
  start:string;
  end:string;
  date:string;
  errMsg : string;
  msg:string;
  capacity: string="0";
  notvalidatedMsg:string="";
  capacityPercentage:string;

  isCapacityLessThan60 : boolean=false;
  ispresent:boolean=false;
  isAttendanceLessThan5:boolean=false;
  isvalidated:boolean=true;
  
  
  ngOnInit(){
    this.feedback.answer1 = "Yes";
   
    this.route.queryParams
    .subscribe(params => {

      this.feedback.eventId  = params.eventId; 
      //console.log(params.eventId);
      this.eventName = params.eventName;
      console.log("hello"+this.eventName);
      this.feedback.club = params.clubName;
      this.feedback.eventType = params.eventType;
      this.start = params.start.substring(0,5);
      this.end = params.end.substring(0,5);
      this.date = params.date;
      this.capacity = params.capacity;
      console.log(params); 
      this.service.checkFeedbackPresence(params.eventId).then(c => this.ispresent = c);
     }
    );
  }


checkAttendance(){
  if(this.capacity=="0"){
    this.capacityPercentage="0";
  }
  else {
    this.capacityPercentage = ((this.feedback.attendees)/parseInt(this.capacity)*100).toFixed(2);
  }  

  this.feedback.capacityPercentage = this.capacityPercentage+"";

  if(this.feedback.attendees<=5 && this.feedback.attendees &&  (this.feedback.eventType=="Fitness" || this.feedback.eventType == "Cooking") ){
    this.isAttendanceLessThan5=true;
  }
  else if(this.feedback.attendees.toString()==""){
    this.isAttendanceLessThan5=false;
  } 
  else{
    this.isAttendanceLessThan5=false;
  }
}


addFeedback() {
    if (this.feedback.attendees<0 ){
        this.isvalidated = false;
    }
   if(isNumeric(this.feedback.attendees)==false )
    {
      this.isvalidated = false;
       
    }
    if(this.feedback.attendees==null){
      this.isvalidated = false;
    }
    if((this.feedback.attendees) )
    if(this.feedback.attendees<=5 && this.feedback.lessAttendanceReason.trim()==""){
      console.log("This the value of attendeess");
      console.log(this.feedback.attendees);
      this.isvalidated=false;
    }

   if (this.feedback.answer1=='No' && this.feedback.howCome.trim()==""){
        this.isvalidated = false;
    }

    if (this.feedback.answer2.trim()==""){
        this.isvalidated = false;
    }  

    if (this.isvalidated){
      if (this.feedback.eventType==""){
        this.feedback.eventType = "Other";
      }

      this.feedback.year = this.date.substring(0,4);
      this.feedback.month = this.date.substring(5,7); 
      this.feedback.day = this.date.substring(8,10); 
      this.feedback.eventName = this.eventName;
      this.service.addInstructorFeedback(this.feedback,this.start,this.end,this.date).then(t => this.errMsg = t);
      this.notvalidatedMsg="";     
       
    }
    else {
      this.isvalidated = true;
      this.notvalidatedMsg = "Please fill all fields marked with asterix"; 
    }
  }


}

