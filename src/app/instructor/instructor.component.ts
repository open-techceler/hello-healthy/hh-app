import { Component,OnInit,ViewChild  } from '@angular/core';
import {VirtuaGymService} from '../services/virtua-gym-service';
import {Club} from '../datamodel/Club';
import {Event} from '../datamodel/Event';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  templateUrl: 'instructor.component.html'
})
export class InstructorComponent implements OnInit {

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  constructor(private service : VirtuaGymService) { 

  }

  clubToSearch : any;

  clubs : Club[] = [];
  events : Event[] = [];

  list = [];

  presence = false;

  ngOnInit() {
    this.service.getClubs(this.list).then(c => this.clubs = c);
    console.log("lisis"+this.list);
  }

 searchEvent(event){
   console.log(this.clubToSearch);
   var clubIdToSearch = -1;
   for (let entry of this.clubs) {
     if (entry.clubName==this.clubToSearch){
       console.log(entry.clubId);
       clubIdToSearch = entry.clubId; 
       break;
     }
  } 

  this.service.getClubEvents(clubIdToSearch).then(e => this.events = e);

 }

  checkFeedbackPresence(index){
     var event = this.events[index];
     
     console.log(event);
  }

 search = (text$: Observable<string>) =>
    text$
    .debounceTime(200).distinctUntilChanged()
    .merge(this.focus$)
    .merge(this.click$.filter(() => !this.instance.isPopupOpen()))
    .map(term => (term === '' ? this.list : this.list.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10));

}
