import { Component,OnInit,ViewChild  } from '@angular/core';
import {VirtuaGymService} from '../services/virtua-gym-service';
import {Club} from '../datamodel/Club';
import {Event} from '../datamodel/Event';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import {InstructorFeedback} from '../datamodel/InstructorFeedback';



@Component({
  templateUrl: 'submitted-feedback-form.component.html'
})
export class SubmittedFeedbackFormComponent implements OnInit {
   
  constructor(private service : VirtuaGymService,private route: ActivatedRoute) {
      this.feedback  = new InstructorFeedback();
  }

  clubs : Club[] = [];
  events : Event[] = [];

  list = [];

  presence = false;

  feedback: InstructorFeedback;
  eventName : string;
  clubName : string;
  start:string;
  end:string;
  date:string;
  errMsg : string;
  msg:string;
  capacity: string;
 
  capacityPercentage:string;

  ngOnInit(){
  
    this.route.queryParams
    .subscribe(params => {
      this.feedback.eventId  = params.eventId; 
      this.eventName = params.eventName;
      this.start = params.start;
      this.end = params.end;
      this.date = params.date;
      console.log("hello"+ this.eventName);
      this.feedback.club = params.clubName;
      console.log(this.feedback.club);   
    });
  }
}
