import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {
  href:string;
  isAdmin : boolean = false;
  isParticipants: boolean = false;
  isInstructor: boolean = false;
  public disabled = false;
  public status: {isopen: boolean} = {isopen: false};
  
  public toggled(open: boolean): void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

 constructor(private router: Router) {}

    ngOnInit() {
        
        this.href = this.router.url;
        
        if (this.href=="/admin" ){
          this.isAdmin = true;
          
        }

        if (this.href=="/participants" ){
          this.isParticipants = true;
          
        }

        if (this.href!="/admin" && this.href!="/participants"){
          this.isInstructor = true;
        }
        
        console.log("url is"+this.router.url);
    }
}
