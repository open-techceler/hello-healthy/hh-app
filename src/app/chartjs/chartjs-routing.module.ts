import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartJSComponent } from './chartjs.component';
import { ClubsComponent } from './clubs.component';

const routes: Routes = [
  {
    path: '',
    component: ChartJSComponent,
    data: {
      title: 'Reports'
    }
  },
  {
    path: '',
    component: ChartJSComponent,
    data: {
      title: 'Management Reports'
    }
  },
  {
    path: 'clubs',
    component: ClubsComponent,
    data: {
      title: 'Club Management'
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartJSRoutingModule {}
