import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { ChartJSComponent } from './chartjs.component';
import { ClubsComponent } from './clubs.component';
import { ChartJSRoutingModule } from './chartjs-routing.module';
import { VirtuaGymService } from 'app/services/virtua-gym-service';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    ChartJSRoutingModule,
    ChartsModule,
    HttpModule,
    FormsModule,
    CommonModule
  ],
  declarations: [ ChartJSComponent,ClubsComponent ],
  providers : [VirtuaGymService]
})
export class ChartJSModule { }
