import { Component,OnInit } from '@angular/core';
import {VirtuaGymService} from '../services/virtua-gym-service';
import {Attendance} from '../datamodel/Attendance';
import {Club} from '../datamodel/Club';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'chartjs.component.html'
})
export class ChartJSComponent implements OnInit {

  constructor(private router: Router,private route : ActivatedRoute,private service : VirtuaGymService){

  }

  // barChart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false
  };

  public barChartLabels: string[] = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL','AUG','SEP','OCT','NOV','DEC'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Classes'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Attendance'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Ave. Attendance'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: '% Capacity'}
  ];

  public data: any[] = [
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Classes'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Attendance'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Ave. Attendance'},
    {data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: '% Capacity'}
  ];

  eventType : string = "All";
  year : string = "2018";
  club : string;
  attendance : Attendance;
  list = [];
  clubs : Club[];
  paramClubName:string;

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  clubSelected () {
    console.log(this.club);
    this.service.getAttendanceData(this.data,this.eventType,this.year,this.club).then(c=>this.data=c);
  }

  eventTypeSelected() {
    console.log(this.eventType);
    this.service.getAttendanceData(this.data,this.eventType,this.year,this.club).then(c=>this.data=c);
}

  yearSelected(){
    console.log(this.year);
    this.service.getAttendanceData(this.data,this.eventType,this.year,this.club).then(c=>this.data=c);
  }
  

  update () {
    let clone = JSON.parse(JSON.stringify(this.data));
    this.barChartData = clone;
  }

  ngOnInit(){
    this.service.getClubs(this.list).then(c => this.clubs = c);

   this.route.params
   .subscribe(params => {
    this.paramClubName = params.clubName;

    console.log(this.paramClubName);
    console.log(this.router.url);
    if (this.router.url.startsWith('/client')){
      this.club = this.paramClubName;
      this.yearSelected();
    }
   else if (this.router.url.startsWith('/admin')){
      this.paramClubName = "All";
      this.club = "All";
   }


   });


  }

}
