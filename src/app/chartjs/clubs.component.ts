import { Component,OnInit,ViewChild,TemplateRef,ViewEncapsulation } from '@angular/core';
import {VirtuaGymService} from '../services/virtua-gym-service';
import {Club} from '../datamodel/Club';
import {Event} from '../datamodel/Event';
import {NgbTypeahead,NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {ManagementTeam} from '../datamodel/MgmtTeam';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  templateUrl: 'clubs.component.html',
  styles: [` .modal-content { left:10px;white;width: 50pc;}`],
  encapsulation: ViewEncapsulation.None
})
export class ClubsComponent implements OnInit {
  closeResult: string;

  constructor(private service : VirtuaGymService,private modalService: NgbModal) { 

  }

  clubToSearch : any;

  clubs : Club[] = [];

  list = [];

  selectedClubId : number;
  
  selectedClubName : string;

  selectedName:string;


  teams : ManagementTeam[] = [];
  
  addable = false;

  newTeam : ManagementTeam = new ManagementTeam();

  add(){
    this.addable = true;
     this.newTeam = new ManagementTeam();
     
  }

  save(){
    this.addable = false;
    this.newTeam.clubId = this.selectedClubId;
      let team = new ManagementTeam();
         team.clubId=19408;
         team.firstName = this.newTeam.firstName;
         team.lastName = this.newTeam.lastName;
         team.position = this.newTeam.position;
         team.email = this.newTeam.email;
         team.phone = this.newTeam.phone;
         this.teams.push(team);   
         this.service.addClubTeamMember(team);
  }

  ngOnInit() {
    this.service.getClubs(this.list).then(c => this.clubs = c);
    
        console.log("lisis"+this.list);
  }

  remove(index : number){
    this.service.removeClubTeamMember(this.teams[index].managerId);

this.teams.splice(index);
  }

  open(content, index : number) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    console.log(index);
    console.log(this.clubs[index].clubName);
    console.log(this.clubs[index].clubId);
    this.selectedClubId = this.clubs[index].clubId;
    
   
    this.selectedClubName = this.clubs[index].clubName;
    
    this.service.getClubMgmtTeam(this.selectedClubId).then(c=>this.teams = c);
    this.addable = false;
    
}

  private getDismissReason(reason: any): string {
    this.addable = false;
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  
}