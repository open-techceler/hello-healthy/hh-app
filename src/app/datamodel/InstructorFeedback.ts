export class InstructorFeedback {
    eventId : string ;
    eventType : string;
    attendees : number;
    answer1: string = "";
    answer2: string ="";
    answer11:string="";
    url : string;
    club : string;
    year : string;
    month: string; 
    eventName: string;
    lessAttendanceReason:string=""; 
    howCome:string="";
    funideas:string;
    capacityPercentage:string;
    day:string;
  

}