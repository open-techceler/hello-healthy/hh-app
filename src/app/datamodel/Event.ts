export class Event {
    eventId : string;
    name : string;
    start: string;
    end:string;
    date: string;
    capacity: string;
    eventType:string = "Unassigned";
    presence : boolean = false;
}