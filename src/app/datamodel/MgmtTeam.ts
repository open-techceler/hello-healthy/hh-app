export class ManagementTeam {
    firstName : string;
    lastName : string;
    managerId : string;
    phone : string;
    position : string;
    email : string;
    clubId : number;
    buildingId:number;
}