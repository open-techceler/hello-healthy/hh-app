import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';


export const routes: Routes = [
 
  
  
  {
    path: '',
    redirectTo: 'instructor/instructor',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      // {
      //   path: 'dashboard',
      //   loadChildren: './dashboard/dashboard.module#DashboardModule'
      // },
   //   {
     //   path: 'client/:clubName',
       // loadChildren: './chartjs/chartjs.module#ChartJSModule'
      //},
      {
        path: 'admin',
        loadChildren: './chartjs/chartjs.module#ChartJSModule',
        data: {
          title: 'Management Reports'
        }
      },
      
    ]

  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    }
  },
  {
    path: 'instructor',
    component: FullLayoutComponent,
    data: {
      title: 'Instructor'
    },
    children: [
      {
        path: '',
        loadChildren: './instructor/instructor.module#InstructorModule',
      },
      
    ]
  },
  {
    path: 'participants',
    component: FullLayoutComponent,
    loadChildren: './participants/participants.module#ParticipantsModule',
    data: {
      title: 'Participants'
    }
  }
  
  ,
  {
    path: '',
    component: SimpleLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'client/:clubName',
        loadChildren: './chartjs/chartjs.module#ChartJSModule'
      }
  ]
}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
